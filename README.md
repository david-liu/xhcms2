# xhcms通用型后台管理系系统
## 如果对你有帮助，请帮忙点个赞
## 官网
http://www.xhadmin.com

### 介绍
基于tp6.0正式版开发的免费cms系统  支持自定义内容模型 自定义表单  cms标签调用

### 演示地址
演示地址 http://cms2.xhadmin.com  后台全部账号 admin 密码 000000


### 运行环境
最低 php7.1
数据库配置 在 .env文件

### 安装说明
解析一个本地host域名 运行目录设置为public 如果是ngnix注意配置好伪静态  数据库文件在根目录data.sql  
后台地址： http://你的域名/admin

### 视频教程
http://www.xhadmin.com/html/shipinjiaocheng/cmsyingyongshengcheng/index.html
本教程是xhadmin的系列教程  其中cms使用操作跟本系统一直 包括标签使用 表单提交等



#### 版权信息
请尊重作者的劳动成果，你可以免费学习并且使用商业项目，非授权版本请保留后台版权信息 如需授权请联系作者
不管是免费版本还是正版授权，禁止在xhcms整体或任何部分基础上发展任何派生版本、修改版本或第三方版本用于重新分发。

# 免责
作者不保证也不能保证您由于使用本软件造成数据的丢失和由于在互联网上使用本软件带来的任何风险担保。作者不对任何情况下由本软件带来的损失承担责任，包括使用本软件、不能使用本软件和书面材料带来的利润损失，或其他附随性的损失和结果性损失。作者也不对由于黑客的进入而导致的数据库损失负责。


作者qq：274363574

交流学习请加qq群
qq群：1054152124

# 特别鸣谢

感谢以下的项目,排名不分先后

ThinkPHP：http://www.thinkphp.cn

Bootstrap：http://getbootstrap.com

H+: http://www.zi-han.net/theme/hplus/

layui: https://www.layui.com/